/* 
Please note! This assumes that you've already loaded in 
the structure of the database via the other SQL file. Without
the table structure, this seed data cannot be added.
*/
	
/* Add in some sample users */
/* John Doe is a normal site user, his password is "password" */
insert into user (
	username,password,
	email,last_login,
	admin,full_name,
	active,ID
) value (
	'johndoe',
	'$2y$10$OaClfzmGnjcz79RsKLe./uYVEvzen.86HZCZ9ayrvP5vFk1z7qCJy',
	'johndoe@sampledomain.com',
	unix_timestamp(),
	0,
	'John Doe',
	1, NULL
);
SET @fuserid:=LAST_INSERT_ID();
/* Ima Admin is an admin, her password is also "password" */
insert into user (
	username,password,
	email,last_login,
	admin,full_name,
	active,ID
) value (
	'imaadmin',
	'$2y$10$StMpCySaSLHuvh3KLFNiZ.vSEykBF8kVWEG6ZtuXzQd4F9WWlfBFi',
	'ima@sampledomain.com',
	unix_timestamp(),
	1,
	'Ima Admin',
	1, NULL
);
SET @suserid:=LAST_INSERT_ID();
/* ----------------------------------*/

